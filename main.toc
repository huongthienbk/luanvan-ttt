\contentsline {section}{\numberline {1}Ph\IeC {\`\acircumflex }n 1 - T\IeC {\h \ocircumflex }ng quan}{7}{section.1}
\contentsline {subsection}{\numberline {1.1}Ph\IeC {\'a}t bi\IeC {\h \ecircumflex }u v\IeC {\'\acircumflex }n \IeC {\dj }\IeC {\`\ecircumflex }}{7}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}M\IeC {\d u}c ti\IeC {\^e}u \IeC {\dj }\IeC {\`\ecircumflex } t\IeC {\`a}i nghi\IeC {\^e}n c\IeC {\'\uhorn }u}{7}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}C\IeC {\'a}c nghi\IeC {\^e}n c\IeC {\'\uhorn }u li\IeC {\^e}n quan}{7}{subsection.1.3}
\contentsline {section}{\numberline {2}Ph\IeC {\`\acircumflex }n 2 - C\IeC {\ohorn } s\IeC {\h \ohorn } l\IeC {\'y} thuy\IeC {\'\ecircumflex }t chung}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}L\IeC {\'y} thuy\IeC {\'\ecircumflex }t}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Lu\IeC {\d \acircumflex }t ch\IeC {\ohorn }i}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Lu\IeC {\d \acircumflex }t t\IeC {\'\i }nh \IeC {\dj }i\IeC {\h \ecircumflex }m}{10}{subsection.2.3}
\contentsline {section}{\numberline {3}Ph\IeC {\`\acircumflex }n 3 - M\IeC {\^o} h\IeC {\`\i }nh tr\IeC {\'\i } tu\IeC {\d \ecircumflex } nh\IeC {\^a}n t\IeC {\d a}o Heuristic}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}C\IeC {\ohorn } s\IeC {\h \ohorn } l\IeC {\'y} thuy\IeC {\'\ecircumflex }t}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}M\IeC {\^o} h\IeC {\`\i }nh}{11}{subsection.3.2}
\contentsline {section}{\numberline {4}Ph\IeC {\`\acircumflex }n 4 - M\IeC {\^o} h\IeC {\`\i }nh m\IeC {\d a}ng MLP - Multi-layer Perceptron}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}C\IeC {\ohorn } s\IeC {\h \ohorn } l\IeC {\'y} thuy\IeC {\'\ecircumflex }t \ref {itm:refmlp}\ref {itm:reflythuyetmlp}}{13}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Hi\IeC {\d \ecircumflex }n th\IeC {\d \uhorn }c}{27}{subsection.4.2}
\contentsline {section}{\numberline {5}Ph\IeC {\`\acircumflex }n 5 - Ki\IeC {\h \ecircumflex }m ch\IeC {\'\uhorn }ng v\IeC {\`a} \IeC {\dj }\IeC {\'a}nh gi\IeC {\'a} gi\IeC {\h a}i thu\IeC {\d \acircumflex }t}{32}{section.5}
\contentsline {subsection}{\numberline {5.1}M\IeC {\^o} h\IeC {\`\i }nh ki\IeC {\h \ecircumflex }m ch\IeC {\'\uhorn }ng}{32}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Ki\IeC {\h \ecircumflex }m ch\IeC {\'\uhorn }ng gi\IeC {\h a}i thu\IeC {\d \acircumflex }t Heuristic}{33}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Ki\IeC {\h \ecircumflex }m ch\IeC {\'\uhorn }ng gi\IeC {\h a}i thu\IeC {\d \acircumflex }t MLP}{36}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}\IeC {\DJ }\IeC {\'a}nh gi\IeC {\'a} gi\IeC {\h a}i thu\IeC {\d \acircumflex }t}{40}{subsection.5.4}
\contentsline {section}{\numberline {6}Ph\IeC {\`\acircumflex }n 6 T\IeC {\h \ocircumflex }ng k\IeC {\'\ecircumflex }t}{40}{section.6}
\contentsline {subsection}{\numberline {6.1}\IeC {\DJ }\IeC {\'a}nh gi\IeC {\'a} k\IeC {\'\ecircumflex }t qu\IeC {\h a}}{40}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}\IeC {\DJ }\IeC {\'o}ng g\IeC {\'o}p c\IeC {\h u}a lu\IeC {\d \acircumflex }n v\IeC {\u a}n}{41}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}H\IeC {\uhorn }\IeC {\'\ohorn }ng ph\IeC {\'a}t tri\IeC {\h \ecircumflex }n}{41}{subsection.6.3}
\contentsline {section}{\numberline {7}T\IeC {\`a}i li\IeC {\d \ecircumflex }u tham kh\IeC {\h a}o}{43}{section.7}
